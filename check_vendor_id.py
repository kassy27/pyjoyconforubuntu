import hidraw

devices = hidraw.enumerate()

for device in devices:
    vendor_id      = device["vendor_id"]
    product_id     = device["product_id"]
    product_string = device["product_string"]
    serial = device.get('serial') or device.get("serial_number")

    print(vendor_id,product_id,product_string,serial)

print ('')


for device in hidraw.enumerate(0, 0):
    for k, v in device.items():
        print ('{} : {}'.format(k, v))
    print ('')


