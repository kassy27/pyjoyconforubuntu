# PYJOYCON For Ubuntu

## OverView

Python driver for Nintendo Switch Joy-Con

[joycon-python](https://github.com/tocoteron/joycon-python) for Ubuntu18.04

We convert import source from hid to hidraw.

## Real environment settings

### Install
```
sudo apt install build-essential libssl-dev libffi-dev python3-dev
sudo apt install libusb-1.0-0-dev libudev-dev
sudo apt install python3-venv
```

### Add file
Create a rules file named "50-nintendo-switch.rules" by referring to the [link](https://www.reddit.com/r/Stadia/comments/egcvpq/using_nintendo_switch_pro_controller_on_linux/fc5s7qm/).

```
# Switch Joy-con (L) (Bluetooth only)
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2006.*", MODE="0666"

# Switch Joy-con (R) (Bluetooth only)
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2007.*", MODE="0666"

# Switch Pro controller (USB and Bluetooth)
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="2009", MODE="0666"
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", KERNELS=="0005:057E:2009.*", MODE="0666"

# Switch Joy-con charging grip (USB only)
KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="200e", MODE="0666"

```

And Copy the above file to "/etc/udev/rules.d".


## Virtual envisonment settings

### Create and activate

```
python3 -m venv joycon
source joycon/bin/activate

```

### Install after activate
```
pip install --upgrade pip
pip install --upgrade setuptools
pip install hidapi pyglm
```

## QuickStart
You can read joycon values.
```
python joycon_read.py
```
## Fun usage example
Exercise to shake L and R Controllers
```
python enjoycon.py
```

## Environments
```
Ubuntu18.04
Python 3.6.9
hidapi 0.7.99.post21
```
