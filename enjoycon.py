# Fun usage example
# Let's shake controllers!!

from .pyjoycon import JoyCon, get_L_id, get_R_id
import time
import math

class EnJoyCon:
    joycon_l = None
    joycon_r = None
    is_connect_l = False
    is_connect_r = False
    shake_condition_l=0.0
    shake_condition_r=0.0
    shake_th = 2000.0
    shake_dec=0.01
    shake_inc=0.1

    acc_th=45000.0 #35000 jab , 45000 straight
    acc_old_list=[0.0, 0.0, 0.0] # Refer to acceleration Number of frames  

    def __init__(self):
        pass

    def connect(self):
        joycon_id_L = get_L_id()
        joycon_id_R = get_R_id()
        ret = True

        try:
            if not self.is_connect_l:
                self.joycon_l = JoyCon(*joycon_id_L)
                self.is_connect_l = True

            if not self.is_connect_r:
                self.joycon_r = JoyCon(*joycon_id_R)
                self.is_connect_r = True

        except ValueError as e:
            print(e)
            ret = False

        return ret

    def isConnect(self):
        if get_L_id() == (None, None, None):
            self.is_connect_l = False

        if get_R_id() == (None, None, None):
            self.is_connect_r = False
        return self.is_connect_l and self.is_connect_r

    def getCommand(self):
        if (self.joycon_r.get_gyro_x() < self.shake_th and 
            self.joycon_r.get_gyro_x() < self.shake_th and 
            self.joycon_r.get_gyro_x() < self.shake_th):

            self.shake_condition_r -= self.shake_dec

            if self.shake_condition_r < 0.0:
                self.shake_condition_r = 0.0

        if (self.joycon_r.get_gyro_x() > self.shake_th and 
            self.joycon_r.get_gyro_x() > self.shake_th and 
            self.joycon_r.get_gyro_x() > self.shake_th):

            self.shake_condition_r += self.shake_inc

            if self.shake_condition_r > 1.0:
                self.shake_condition_r = 1.0

        ax = self.joycon_l.get_accel_x()
        ay = self.joycon_l.get_accel_y()
        az = self.joycon_l.get_accel_z()
        ad = int(math.sqrt(ax*ax+ay*ay+az*az))
        self.acc_old_list.append(ad)
        self.acc_old_list.pop(0)
        acc_sum = sum(self.acc_old_list)
        # print(self.acc_old_list)
        # print(acc_sum)

        return (acc_sum >= self.acc_th, self.shake_condition_r)

    def getControlStatus(self):
        var = []
        try:
            var_l = [self.joycon_l.get_accel_x(), 
                    self.joycon_l.get_accel_y(),
                    self.joycon_l.get_accel_z(),
                    self.joycon_l.get_gyro_x(),
                    self.joycon_l.get_gyro_y(),
                    self.joycon_l.get_gyro_z(),
                    self.joycon_l.get_stick_left_horizontal(),
                    self.joycon_l.get_stick_left_vertical(),
                    self.joycon_l.get_button_down(),
                    self.joycon_l.get_button_up(),
                    self.joycon_l.get_button_right(),
                    self.joycon_l.get_button_left(),
                    self.joycon_l.get_button_left_sr(),
                    self.joycon_l.get_button_left_sl(),
                    self.joycon_l.get_button_l(),
                    self.joycon_l.get_button_zl(),
                    self.joycon_l.get_button_minus(),
                    self.joycon_l.get_button_l_stick(),
                    self.joycon_l.get_button_capture(),
                    self.joycon_l.get_button_zl(),
                    self.joycon_l.get_button_zl(),
                    self.joycon_l.get_battery_charging(),
                    self.joycon_l.get_battery_level(),
            ]
            var_r = [self.joycon_r.get_accel_x(), 
                    self.joycon_r.get_accel_y(),
                    self.joycon_r.get_accel_z(),
                    self.joycon_r.get_gyro_x(),
                    self.joycon_r.get_gyro_y(),
                    self.joycon_r.get_gyro_z(),
                    self.joycon_r.get_stick_right_horizontal(),
                    self.joycon_r.get_stick_right_vertical(),
                    self.joycon_r.get_button_plus(),
                    self.joycon_r.get_button_r_stick(),
                    self.joycon_r.get_button_home(),
                    self.joycon_r.get_button_y(),
                    self.joycon_r.get_button_x(),
                    self.joycon_r.get_button_b(),
                    self.joycon_r.get_button_a(),
                    self.joycon_r.get_button_right_sr(),
                    self.joycon_r.get_button_right_sl(),
                    self.joycon_r.get_button_r(),
                    self.joycon_r.get_button_zr(),
                    self.joycon_r.get_battery_charging(),
                    self.joycon_r.get_battery_level(),
                    ]
            var.extend(var_l)
            var.extend(var_r)
        except ValueError as e:
            print(e)

        return var



if __name__ == '__main__':
    enjoy = EnJoyCon()
    enjoy.connect()

    while True:
        # print(enjoy.getControlStatus())
        print(enjoy.getCommand())
        if not enjoy.isConnect():
            enjoy.connect()
        time.sleep(0.1)